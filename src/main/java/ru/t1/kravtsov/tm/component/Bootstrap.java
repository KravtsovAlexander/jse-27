package ru.t1.kravtsov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.kravtsov.tm.api.component.IBootstrap;
import ru.t1.kravtsov.tm.api.repository.ICommandRepository;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.api.service.*;
import ru.t1.kravtsov.tm.command.AbstractCommand;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kravtsov.tm.exception.system.CommandNotSupportedException;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.repository.CommandRepository;
import ru.t1.kravtsov.tm.repository.ProjectRepository;
import ru.t1.kravtsov.tm.repository.TaskRepository;
import ru.t1.kravtsov.tm.repository.UserRepository;
import ru.t1.kravtsov.tm.service.*;
import ru.t1.kravtsov.tm.util.SystemUtil;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IBootstrap, IServiceLocator {

    @NotNull
    private final static String PACKAGE_COMMANDS = "ru.t1.kravtsov.tm.command";

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz);
        }
    }

    @Override
    public void run(@Nullable String[] args) {
        prepareStartup();
        if (parseArguments(args)) exit();
        parseCommands();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        LOGGER_LIFECYCLE.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
    }

    private void prepareShutdown() {
        backup.stop();
        fileScanner.stop();
        LOGGER_LIFECYCLE.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(user.getId(), new Project("BETA PROJECT", Status.NOT_STARTED));
        projectService.add(user.getId(), new Project("ALPHA PROJECT", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("GAMMA PROJECT", Status.COMPLETED));

        taskService.add(user.getId(), new Task("DELTA TASK"));
        taskService.add(admin.getId(), new Task("EPSILON TASK"));
    }

    private boolean parseArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;

        @Nullable final String arg = args[0];
        parseArgument(arg);
        return true;
    }

    private void parseCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                parseCommand(command);
                LOGGER_COMMANDS.info(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseArgument(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(arg);
        if (command == null) throw new ArgumentNotSupportedException(arg);
        command.execute();
    }

    public void parseCommand(@Nullable final String command) {
        parseCommand(command, true);
    }

    public void parseCommand(@Nullable final String command, final boolean checkRoles) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) {
            authService.checkRoles(abstractCommand.getRoles());
        }
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        registry(clazz.newInstance());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
