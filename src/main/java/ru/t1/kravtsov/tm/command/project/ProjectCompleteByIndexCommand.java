package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Complete project by index.";

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(getUserId(), index, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
