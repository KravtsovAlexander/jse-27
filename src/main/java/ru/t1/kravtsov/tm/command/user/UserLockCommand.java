package ru.t1.kravtsov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Lock user.";

    @NotNull
    public static final String NAME = "user-lock";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
